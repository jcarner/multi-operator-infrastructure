# Multi-operator infrastructure #

This repository is for building a multi-operator infrastructure using Kamailio, an open-source SIP server. Kamailio is used in real-world large deployments in operator networks.

## Scenario ##

Welcome to our imaginary world. As you may know, the United Archipelago Republic is formed by morethan 100 islands. However, 99% of the population enjoy their time in the two biggest islands, namely, BigBanana and Big Treasure Islands.The national telecommunications operator has been acquired by a Catalan capital risk fund in 2014. Foroperative reasons, it’s been decided to divide the old operator in two smaller operators, each based inone of the main islands: Big Banana Telecom and Big Treasure Telecom.This Caribbean country provides very good conditions when it comes to tax conditions. So, given the global financial crisis, lots of companies are moving there and the country is increasing its GDP and population. The existing telephony network is getting obsolete and in order to renew it while handling with the new capacity demand, the CTO is now setting up a team of rockstar engineers to design and deploy a brand new VoIP network. You have been luckily selected for this job; so pack your luggage (don’t forget your surfboard!) and enjoy the experience.

Users in the island access the network using WiMAX radio links. Unfortunately, access capacity
dramatically decreases during hurricanes and other extreme atmospheric phenomena. For this reason, in
the past it was decided to use a low-quality/consumption codec, like GSM. So far this has been working
quite well, but our new CEO, Dr. Peck Eño-Ni Colas, decided that we should offer a better audio quality
when the sky is blue and the Sun is shining, which is almost all the time. To do so, we’ll force GSM codec
in our calls only when wind is over 50km/h or visibility is lower than 3km. To

## Functionality ##

For each operator (Big Banana, Big Treasure) a Kamailio server should be configured so that it can do the following:

1. Register two users. No need to provide authentication.

    a. Two users (A, B) will register at Big Banana server

    b. Two users (C, D) will register at Big Treasure server

2. Establish calls between two registered users. No need to provide authentication. 

    a. Two users belonging to the same server/island should be able to call each other

3. Establish calls to the other operator/island. No need to provide authentication.

     a. Two users belonging to different servers/islands should be able to call each other

Text from the Final Lab of Multimedia Transmission Protocols (November 2014), taught by **Victor Pascual Ávila**.

## Configuration ##

* Big Banana : **10.80.138.200**
* Big Treasure: **10.80.138.202**